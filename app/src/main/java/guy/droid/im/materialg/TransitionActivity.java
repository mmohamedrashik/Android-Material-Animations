package guy.droid.im.materialg;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.PathMotion;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.Visibility;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransitionActivity extends AppCompatActivity {

    @BindView(R.id.toobar)
    Toolbar toolbar;

    @BindView(R.id.trans_exit)
    Button texit;
    Constants.TransitionType type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition);
        ButterKnife.bind(this);
        toolbar.setTitle("RIPPLE");

        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLUE);
        }
        initPage();


            initAnimation();

    }

    @Override
    public boolean onSupportNavigateUp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             finishAfterTransition();
        }
        return true;
    }

    public void initPage()
    {
        type = (Constants.TransitionType)getIntent().getSerializableExtra(Constants.KEY_ANIM_TYPE);
        toolbar.setTitle(getIntent().getSerializableExtra(Constants.KEY_TITLE).toString());
        texit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                }
            }
        });
    }

    public void initAnimation()
    {
        switch (type)
        {
            case ExplodeJava:{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Explode explode = new Explode();
                    explode.setDuration(1000);
                   // explode.setMode(Visibility.MODE_IN);
                    getWindow().setEnterTransition(explode);
                    getWindow().setExitTransition(new Explode());
                }

                break;
            }

            case ExplodeXML:{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Transition entertransition = TransitionInflater.from(this).inflateTransition(R.transition.explode);
                    getWindow().setEnterTransition(entertransition);

                }

                break;
            }

            case SlideJava:{

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Slide slide = new Slide();
                  //  toolbar.setBackgroundColor(Color.RED);
                    slide.setSlideEdge(Gravity.TOP);
                    slide.setDuration(1000);

                    slide.setInterpolator(new AnticipateInterpolator());
                    getWindow().setEnterTransition(slide);

                }
                break;
            }

            case SlideXml:{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Transition entertransition = TransitionInflater.from(this).inflateTransition(R.transition.slide);
                    getWindow().setEnterTransition(entertransition);

                }

                break;
            }

            case FadeJava:{

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Fade fade = new Fade();
                    fade.setDuration(1000);

                    fade.setInterpolator(new LinearInterpolator());
                    getWindow().setEnterTransition(fade);
                    getWindow().setExitTransition(new Fade().setDuration(300).setInterpolator(new LinearInterpolator()));

                }
                break;
            }

            case FadeXML:{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Transition entertransition = TransitionInflater.from(this).inflateTransition(R.transition.fade);
                    getWindow().setEnterTransition(entertransition);
                    getWindow().setExitTransition(entertransition);

                }

                break;
            }
        }
    }
}
