package guy.droid.im.materialg;

/**
 * Created by admin on 3/31/2017.
 */

public class Constants {

    public static final String KEY_ANIM_TYPE = "animtype";
    public static final String KEY_TITLE = "anim_title";

    public enum TransitionType
    {
        ExplodeJava,ExplodeXML,SlideJava,SlideXml,FadeJava,FadeXML;
    }
}
