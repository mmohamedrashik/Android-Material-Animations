package guy.droid.im.materialg;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Shared_Element_anim extends AppCompatActivity {
    Toolbar toolbar;

    @BindView(R.id.exit_button)
    Button exit_button;

    @BindView(R.id.sometextmain)
    TextView main;

    @BindView(R.id.main2)
    TextView main2;

    @BindView(R.id.revealer)
    RelativeLayout relativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared__element_anim);

        ButterKnife.bind(this);
        toolbar = (Toolbar)findViewById(R.id.toobar);
        toolbar.setTitle("APP");
        toolbar.setSubtitle("Material App Design");
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_revert);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLUE);
        }

        initPage();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           finishAfterTransition();
        }
        return true;
    }

    public void initPage()
    {


        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                   finishAfterTransition();

                }
            }
        });

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   relativeLayout.setVisibility(View.VISIBLE);
                makeCircularRevealAnimation(relativeLayout);

            }
        });

        main2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  relativeLayout.setVisibility(View.INVISIBLE);
                makeCircularRevealAnimation(relativeLayout);
            }
        });


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void makeCircularRevealAnimation(View views)
    {
        final View view = views;

// get the center for the clipping circle
        int centerX = (view.getLeft() + view.getRight()) / 2;
        int centerY = (view.getTop() + view.getBottom()) / 2;

        int startRadius = 0;
// get the final radius for the clipping circle
        int endRadius = Math.max(view.getWidth(), view.getHeight());

// create the animator for this view (the start radius is zero)
        Animator anim = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, startRadius, endRadius);

// make the view visible and start the animation


        if(view.getVisibility() == View.VISIBLE)
        {
            Animator anims = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, endRadius, startRadius);
            anims.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
            anims.start();
        }else
        {

            anim.start();
            view.setVisibility(View.VISIBLE);
        }

    }
}
