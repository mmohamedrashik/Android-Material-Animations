package guy.droid.im.materialg;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
Toolbar toolbar;

    @BindView(R.id.ripple_button)
    Button button;

    @BindView(R.id.sample)
    TextView txtshared;

    @BindView(R.id.logos)
    ImageView logos;

    @BindView(R.id.profile)
    ImageView profile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbar = (Toolbar)findViewById(R.id.toobar);
        toolbar.setTitle("APP");
        toolbar.setSubtitle("Material App Design");
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_revert);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLUE);
        }

    }

    @OnClick(R.id.ripple_button) void submit()
    {
        startActivity(new Intent(getApplicationContext(),Ripple.class));
    }

    public void SharedElementTransition(View view)
    {
      //  startActivity(new Intent(getApplicationContext(),Shared_Element_anim.class));

        Pair[] pair = new Pair[3];
        pair[0] = new Pair<View,String>(logos,"logoshared");
        pair[1] = new Pair<View,String>(txtshared,"sampleshared");
        pair[2] = new Pair<View,String>(profile,"profileshared");

        Intent intent = new Intent(MainActivity.this,Shared_Element_anim.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,pair);
            startActivity(intent,options.toBundle());
        }

    }

    public void explodebyjava(View view)
    {
       // startActivity(new Intent(getApplicationContext(),TransitionActivity.class));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.ExplodeJava);
            i.putExtra(Constants.KEY_TITLE,"ExplodeByJava");
            startActivity(i,activityOptions.toBundle());
        }
    }
    public void explodebyxml(View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.ExplodeXML);
            i.putExtra(Constants.KEY_TITLE,"ExplodeByXML");
            startActivity(i,activityOptions.toBundle());
        }
    }

    public void slidebyjava(View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.SlideJava);
            i.putExtra(Constants.KEY_TITLE,"Slide Java");
            startActivity(i,activityOptions.toBundle());
        }
    }
    public void slidebyxml(View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.SlideXml);
            i.putExtra(Constants.KEY_TITLE,"Slide XML");
            startActivity(i,activityOptions.toBundle());
        }
    }

    public void fadebyjava(View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.FadeJava);
            i.putExtra(Constants.KEY_TITLE,"Fade Java");
            startActivity(i,activityOptions.toBundle());
        }
    }
    public void fadebyxml(View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent i = new Intent(getApplicationContext(),TransitionActivity.class);
            i.putExtra(Constants.KEY_ANIM_TYPE,Constants.TransitionType.FadeXML);
            i.putExtra(Constants.KEY_TITLE,"Fade XML");
            startActivity(i,activityOptions.toBundle());
        }
    }
}
